# **PCL Localizer**.
*A simply cross platform alternative to Resx.*
[Follow this guide step by step!](http://markjackmilian.net/blog/pcllocalizer-step-by-step/) [OLD]

#*Quick Start*
*Add [Pcl.Localizer](https://www.nuget.org/packages/Pcl.Localizer/) nuget package to all projects that will use localization.*
In your entry point app call:

	PclResMan.SetLanguage(yourlanguage);

**How generate resources:**
You can use PclLocalization.Console or add [Pcl.Localization.Generator](https://www.nuget.org/packages/Pcl.Localizer.Generator.Excel/) nuget package to the project that will contains the resources.
The nuget will create 3 folders:

 1. Generator [Console Generator & runner.bat]	     
 2. Generated [Generated classes resources]
 3. Resources [Origin excel resources file]

When you run the bat the files in resource folder will be parsed and a cs file will be generate..now simply include the new cs file to your project. You can call your resource like this:

	MyLoc.Test

For help about console call:

	PclLocalizer.Console.exe -h

Example for generate:

	PclLocalizer.Console.exe -f "example.xlsx example.en.xlsx" -d MyLoc.cs -n MyNameSpace -c MyLoc -t
where

 - -f is the input file
 - -d is the destination file
 - -n is the namespace for generated class
 - -c is the name of the generated class
 - -t is for thread mode (generated classes will check language from CurrentThread culture), if not used it will check only on PclResMan settings.

For add "Run batch" from Visual Studio context menu follow [this](http://stackoverflow.com/questions/5605885/how-to-run-a-bat-from-inside-the-ide) guide.

##Follow Me

 - Twitter: [@markjackmilian](https://twitter.com/markjackmilian)
 - MyBlog: [markjackmilian.net](http://markjackmilian.net/blog)
 - Linkedin: [linkedin](https://www.linkedin.com/in/marco-giacomo-milani)

##License
Licensed under Apache see license file.