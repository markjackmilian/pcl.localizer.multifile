﻿using System.Collections.Generic;
using System.Linq;

namespace PclLocalizer.Console
{
    class ParameterExtractor
    {
        private readonly IList<string> _arguments;

        public ParameterExtractor(IEnumerable<string> arguments )
        {
            this._arguments = arguments.ToList();
        }


        public IList<string> InputFiles => this.GetValueList(Constants.InputParam);

        public string DestinationFile => this.GetValue(Constants.DestinationParam);
        public string NameSpace => this.GetValue(Constants.NamespaceParam);
        public string ClassName => this.GetValue(Constants.ClassNameParam);
        public bool IsThreadMode => this._arguments.IndexOf(Constants.ThreadParam) != -1;

        private string GetValue(string param)
        {
            var index = this._arguments.IndexOf(param);
            return this._arguments[index + 1];
        }

        private List<string> GetValueList(string param)
        {
            var index = this._arguments.IndexOf((param));
            return this._arguments[index + 1].Split(' ').ToList();
        }
    }
}
