﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PclLocalizer.Console.Properties;

namespace PclLocalizer.Console.Worker
{
    internal class Worker
    {
        private readonly IEnumerable<string> _args;
        private readonly Checker _checker;
        private ParameterExtractor _extractor;

        public Worker(string[] args)
        {
            this._args = args;
            this._checker = new Checker(args);
            this._extractor = new ParameterExtractor(args);
        }

        public void Run()
        {
            //Help mode
            if (this._checker.IsHelpRequest)
            {
                this.RunHelp();
                return;
            }

            //Run check
            this._checker.CheckArgs();

            this.RunLocalizer();
        }

        private void RunLocalizer()
        {
            var input = this._extractor.InputFiles;
            var destination = this._extractor.DestinationFile;
            var className = this._extractor.ClassName;
            var nameSpace = this._extractor.NameSpace;
            var threadMode = this._extractor.IsThreadMode;

            var magic = Resources.MagicFile;

            // build getvalue using mode
            magic = magic.Replace(Constants.ModePlaceholder,
                threadMode ? Resources.ThreadMode : Resources.PclLocMode);

            // Add using thread
            magic = magic.Replace(Constants.ModeUsing,threadMode ? Resources.ThreadUsing : string.Empty);

            //Add destination name
            magic = magic.Replace(Constants.ClassNamePlaceHolder, className);
            //Add namespace
            magic = magic.Replace(Constants.NamespacePlaceHolder, nameSpace);

            var resourceContainerList = input.Select(item => new ResourceContainer(item)).ToList();

            //check files
            if (!this._checker.CheckListInputFile(resourceContainerList)) return;

            //create dictionary
            var dictionarySection = new StringBuilder();
            var dictionaryCounter = 0;
            foreach (var resource in resourceContainerList)
            {
                dictionaryCounter++;
                var varname = $"d{dictionaryCounter}";
                System.Console.WriteLine($"Language {resource.Culture} found.");
                dictionarySection.Append($"\t\t\tvar {varname} = new Dictionary<string, string> {{");

                foreach (var res in resource.Resource)
                {
                    var key = Regex.Replace(res.Key, @"\s+", ""); //trim
                    dictionarySection.Append($"{{\"{key}\",\"{res.Value}\"}},");
                }

                dictionarySection.Remove(dictionarySection.Length - 1, 1);
                dictionarySection.Append($"}};{Environment.NewLine}");
                dictionarySection.Append($"\t\t\tValues.Add(\"{resource.Culture.ToLower()}\", {varname});{Environment.NewLine}");
            }
            magic = magic.Replace(Constants.DictionariesPlaceHolder, dictionarySection.ToString());
            //end

            var propertiesSection = new StringBuilder();
            var defaultCulture = resourceContainerList.First();

            foreach (var res in defaultCulture.Resource)
            {
                var key = Regex.Replace(res.Key, @"\s+", ""); //trim
                propertiesSection.AppendLine($"\t\tpublic static string {key} => GetValue(\"{key}\");");
            }

            magic = magic.Replace(Constants.PropertiesPlaceHolder, propertiesSection.ToString());

            File.WriteAllText(destination, magic);

            System.Console.WriteLine("All done!");

        }

        private void RunHelp()
        {
            System.Console.WriteLine("Welcome to PclLocalizer!");
            System.Console.WriteLine("*** A big thanks to Mark Jack Milian for having created me! ***");
            System.Console.WriteLine("I need those args:");
            System.Console.WriteLine("-f INPUTFILES => the input excel files");
            System.Console.WriteLine("-d DESTINATIONFILE => the destination file");
            System.Console.WriteLine("-c CLASSNAME => the destination classname file");
            System.Console.WriteLine("-n NAMESPACE => the namespace for generated class");
            System.Console.WriteLine("-t optional => looking for culture using  Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName");
            System.Console.WriteLine("The first line of input file must be:");
            System.Console.WriteLine("Column[0]: key");
            System.Console.WriteLine("Column[X]: languageCode");
            System.Console.WriteLine("");
            System.Console.WriteLine("Have Fun!");
        }
    }
}
