﻿using System.Collections.Generic;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace PclLocalizer.Console
{
    class ResourceContainer
    {
        public string Culture { get; set; }
        public Dictionary<string, string> Resource { get; set; }

        public ResourceContainer(string filePath)
        {
            this.Resource = new Dictionary<string, string>();

            var workBook = new XSSFWorkbook(filePath);

            ISheet sheet = workBook.GetSheetAt(0);
            this.Culture = sheet.GetRow(0)?.Cells[1].StringCellValue;

            for (int i = 1; i <= sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);
                if (row != null) 
                {
                    var key = row.Cells[0].StringCellValue;
                    var value = row.Cells[1].StringCellValue.Replace("\n", string.Empty).Replace("\"", "\\\"");
                    this.Resource.Add(key,value);
                }
            }
        }
    }
}