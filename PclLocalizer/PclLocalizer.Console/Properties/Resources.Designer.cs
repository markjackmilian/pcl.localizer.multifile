﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PclLocalizer.Console.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PclLocalizer.Console.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to using System.Collections.Generic;
        ///using System.Linq;
        ///
        ///{{usingMode}}
        ///
        ///using PclLocalizer.ResMan;
        ///
        ///namespace {{namespace}}
        ///{
        ///    /// &lt;summary&gt;
        ///    /// This class is autogenerated from Pcl.Localization Tool.
        ///    /// &lt;/summary&gt;
        ///    public static class {{classname}}
        ///    {
        ///        private static readonly Dictionary&lt;string,Dictionary&lt;string,string&gt;&gt; Values = new Dictionary&lt;string, Dictionary&lt;string, string&gt;&gt;();
        ///
        ///        static {{classname}}()
        ///        {
        ///{{dictionaries}}            
        ///        }
        ///
        ///{ [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MagicFile {
            get {
                return ResourceManager.GetString("MagicFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to var cult = culture ?? PclResMan.Lang;
        ///            if (cult != null &amp;&amp; Values.ContainsKey(cult))
        ///                return Values[cult][key];
        ///            else
        ///            {
        ///                return PclResMan.Default == null ? Values[Values.First().Key][key] : Values[PclResMan.Default][key];
        ///            }.
        /// </summary>
        internal static string PclLocMode {
            get {
                return ResourceManager.GetString("PclLocMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to var currentCulture = !string.IsNullOrWhiteSpace(culture)
        ///                ? culture.ToLower()
        ///                : Thread.CurrentThread.CurrentUICulture.Name.ToLower();
        ///
        ///            // check culture with 5 chars
        ///            if (!string.IsNullOrEmpty(currentCulture) &amp;&amp; Values.ContainsKey(currentCulture))
        ///                return Values[currentCulture][key];
        ///
        ///            // check culture with 2 chars
        ///            if (!string.IsNullOrEmpty(currentCulture) &amp;&amp; Values.Keys.Select(s =&gt; s.Substring(0, 2)).Contain [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ThreadMode {
            get {
                return ResourceManager.GetString("ThreadMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to using System.Threading;.
        /// </summary>
        internal static string ThreadUsing {
            get {
                return ResourceManager.GetString("ThreadUsing", resourceCulture);
            }
        }
    }
}
